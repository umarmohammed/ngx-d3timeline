import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { TimelineDraggingEvent } from './timeline-dragging-event.model';
import { TimelineEvent, mockData } from './timeline-event.model';
import {
  scan,
  distinctUntilChanged,
  pairwise,
  filter,
  map,
  withLatestFrom
} from 'rxjs/operators';

const placeholderEventId = 'NGX_D3TIMELINE_PLACEHOLDER';

type direction = 1 | -1;

// TODO: refactor everything here

function placeholderFromEvent(event: TimelineEvent) {
  return {
    id: placeholderEventId,
    start: event.start,
    finish: event.finish,
    series: event.series,
    type: event.type,
    placeholder: true // TODO: can remove
  };
}

function switchSeries(
  seriesFrom: string,
  dragging: TimelineDraggingEvent,
  data: TimelineEvent[]
): TimelineEvent[] {
  const draggingData = data.find(d => d.id === dragging.dataId);
  const duration = +draggingData.finish - +draggingData.start;
  const fromPlaceholder = data.find(d => d.id === placeholderEventId);
  const seriesFromData = data
    .filter(d => d.series === seriesFrom && d.id !== placeholderEventId)
    .map(d =>
      d.start < fromPlaceholder.start || d.id === draggingData.id
        ? d
        : {
            ...d,
            start: new Date(+d.start - duration),
            finish: new Date(+d.finish - duration)
          }
    );

  const seriesOther = data.filter(
    d => d.series !== seriesFrom && d.series !== dragging.series
  );

  const seriesData = data.filter(d => d.series === dragging.series);
  const diffs = seriesData.map(d => Math.abs(+d.start - +dragging.pointerDate));
  const insertIndex = diffs.indexOf(Math.min(...diffs));
  const placeholder = {
    ...placeholderFromEvent(seriesData[insertIndex]),
    finish: new Date(+seriesData[insertIndex].start + duration),
    type: draggingData.type
  };

  const origData =
    draggingData.series === dragging.series ? [draggingData] : [];

  return [
    ...seriesFromData,
    ...seriesData.slice(0, insertIndex).filter(d => d.id !== draggingData.id),
    placeholder,
    ...seriesData
      .slice(insertIndex, seriesData.length)
      .filter(d => d.id !== draggingData.id)
      .map(d => ({
        ...d,
        start: new Date(+d.start + duration),
        finish: new Date(+d.finish + duration)
      })),
    ...origData,
    ...seriesOther
  ].sort((a, b) => +a.start - +b.start);
}

function movePlaceholder(
  data: TimelineEvent[],
  dragging: TimelineDraggingEvent,
  direction: direction
): TimelineEvent[] {
  const seriesData = data.filter(d => d.series === dragging.series);

  if (seriesData.length < 2) {
    return seriesData;
  }

  let placeholder = seriesData.find(d => d.id === placeholderEventId);
  const placeholderIndex = seriesData.findIndex(
    d => d.id === placeholderEventId
  );
  const draggingDataIndex = seriesData.findIndex(d => d.id === dragging.dataId);

  if (
    (direction > 0 &&
      (placeholderIndex === seriesData.length - 1 ||
        (placeholderIndex === seriesData.length - 2 &&
          draggingDataIndex === seriesData.length - 1))) ||
    (direction < 0 &&
      (placeholderIndex === 0 ||
        (placeholderIndex === 1 && draggingDataIndex === 0)))
  ) {
    return data;
  }

  // TODO: can be computed in constructor of dragging event
  const placeholderDuration =
    direction * +placeholder.finish - direction * +placeholder.start;

  const nextDataItem = seriesData[placeholderIndex + direction];

  let itemToSwap =
    nextDataItem.id !== dragging.dataId
      ? nextDataItem
      : seriesData[placeholderIndex + 2 * direction];
  const itemToSwapDuration =
    direction * +itemToSwap.finish - direction * +itemToSwap.start;

  placeholder = {
    ...placeholder,
    start: new Date(+placeholder.start + itemToSwapDuration),
    finish: new Date(+placeholder.finish + itemToSwapDuration)
  };

  itemToSwap = {
    ...itemToSwap,
    start: new Date(+itemToSwap.start - placeholderDuration),
    finish: new Date(+itemToSwap.finish - placeholderDuration)
  };

  const reorderedSeriesData = seriesData.map((d, i) =>
    i === placeholderIndex
      ? itemToSwap
      : seriesData[i].id === itemToSwap.id
      ? placeholder
      : d
  );

  let resultsIndex = 0;
  return data.map(element =>
    element.series === dragging.series
      ? reorderedSeriesData[resultsIndex++]
      : element
  );
}

@Injectable({ providedIn: 'root' })
export class DragService {
  private draggingSubject = new Subject<TimelineDraggingEvent>();
  private placeholderEventSubject = new Subject<TimelineEvent>();
  private sortedData = [...mockData].sort((a, b) => +a.start - +b.start);

  placeholderWithData$ = this.draggingSubject.pipe(
    pairwise(),
    filter(([prev, curr]) => !!prev || !!curr),
    scan<[TimelineDraggingEvent, TimelineDraggingEvent], TimelineEvent[]>(
      (dataWithPlaceholder, [prevDragging, dragging]) => {
        if (!dataWithPlaceholder) {
          return [
            ...this.sortedData,
            placeholderFromEvent(
              this.sortedData.find(d => d.id === dragging.dataId)
            )
          ].sort((a, b) => +a.start - +b.start);
        }
        if (!dataWithPlaceholder.find(d => d.id === placeholderEventId)) {
          return [
            ...dataWithPlaceholder,
            placeholderFromEvent(
              dataWithPlaceholder.find(d => d.id === dragging.dataId)
            )
          ].sort((a, b) => +a.start - +b.start);
        }

        if (
          prevDragging &&
          dragging &&
          prevDragging.series !== dragging.series
        ) {
          return switchSeries(
            prevDragging.series,
            dragging,
            dataWithPlaceholder
          );
        }

        if (!dragging) {
          // TODO: use dictionaries here and in other find methods
          const draggingData = dataWithPlaceholder.find(
            d => d.id === prevDragging.dataId
          );
          const placeholder = dataWithPlaceholder.find(
            d => d.id === placeholderEventId
          );

          return (
            placeholder &&
            dataWithPlaceholder
              .map(d =>
                d.id === draggingData.id
                  ? {
                      ...draggingData,
                      start: placeholder.start,
                      finish: placeholder.finish,
                      series: placeholder.series
                    }
                  : d
              )
              .filter(d => d.id !== placeholderEventId)
              .sort((a, b) => +a.start - +b.start)
          );
        } else {
          const placeholder = dataWithPlaceholder.find(
            d => d.id === placeholderEventId
          );

          if (
            dragging.pointerDate > placeholder.finish &&
            dragging.direction > 0
          ) {
            return movePlaceholder(dataWithPlaceholder, dragging, 1);
          }

          if (
            dragging.pointerDate < placeholder.start &&
            dragging.direction < 0
          ) {
            return movePlaceholder(dataWithPlaceholder, dragging, -1);
          }

          return dataWithPlaceholder;
        }
      },
      null
    ),
    distinctUntilChanged()
  );

  placeholder$ = this.placeholderWithData$.pipe(
    map(d => d.find(e => e.id === placeholderEventId))
  );

  draggingEvent$ = this.draggingSubject.pipe(
    withLatestFrom(this.placeholderWithData$),
    map(([dragging, placeholderWithData]) =>
      dragging
        ? {
            dragging,
            data: placeholderWithData.find(d => d.id === dragging.dataId)
          }
        : null
    )
  );

  dragStart(data: TimelineEvent) {
    this.placeholderEventSubject.next(data);
  }

  dragged(event: TimelineDraggingEvent) {
    this.draggingSubject.next(event);
  }

  dragEnd() {
    this.resetDraggingState();
  }

  private resetDraggingState() {
    this.draggingSubject.next(null);
  }
}
