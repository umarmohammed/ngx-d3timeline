export type Complete<T> = {
  [P in keyof T]-?: T[P] extends (infer U)[]
    ? Complete<U>[]
    : T[P] extends object
    ? Complete<T[P]>
    : T[P];
};
export type CompleteF<T> = {
  [P in keyof T]-?: T[P] extends (infer U)[]
    ? Complete<U>[]
    : T[P] extends object
    ? CompleteF<T[P]>
    : T[P];
};

export function isObject(item) {
  return item && typeof item === 'object' && !Array.isArray(item);
}

export function mergeDeep(target, source) {
  let output = Object.assign({}, target);
  if (isObject(target) && isObject(source)) {
    Object.keys(source).forEach(key => {
      if (isObject(source[key])) {
        if (!(key in target)) Object.assign(output, { [key]: source[key] });
        else output[key] = mergeDeep(target[key], source[key]);
      } else {
        Object.assign(output, { [key]: source[key] });
      }
    });
  }
  return output;
}
