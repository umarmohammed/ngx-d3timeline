import {
  Component,
  Input,
  Output,
  EventEmitter,
  HostListener,
  AfterViewInit,
  ViewChild,
  ElementRef,
  OnInit,
  SimpleChanges,
  OnChanges
} from '@angular/core';
import { TimelineEvent } from '../timeline-event.model';
import { OptionsService } from '../options/options.service';
import { TextDisplay, TextAlign } from '../options/options';
import { select, event } from 'd3-selection';
import { drag } from 'd3-drag';
import { DragService } from '../drag.service';
import { ZoomService } from '../zoom.service';

// TODO: move somewhere
function scaleBandInvert(scale) {
  var domain = scale.domain();
  var paddingOuter = scale(domain[0]);
  var eachBand = scale.step();
  return function(value) {
    var index = Math.floor((value - paddingOuter) / eachBand);
    return domain[Math.max(0, Math.min(index, domain.length - 1))];
  };
}

@Component({
  selector: 'g[ngx-d3timeline-bar-series]',
  template: `
    <svg:g
      #barEl
      class="bar"
      [attr.transform]="transformString()"
      [ngClass]="data.type"
      [class.active]="activeId === data.id"
      [class.selected]="selectedId === data.id"
      [class.placeholder]="data.placeholder"
      [class.dragging]="dragging"
      [class.zooming]="zoomService.isZooming$ | async"
      (click)="select.emit(data)"
    >
      <svg:rect
        class="bar-series"
        [attr.width]="barWidth(data)"
        [attr.height]="barHeight(data)"
        [attr.stroke-width]="strokeWidth"
        [attr.transform]="optionsService.eventBarTransform"
      ></svg:rect>
      <svg:foreignObject
        [attr.width]="getDivWidth(data)"
        [attr.height]="getDivHeight(data)"
        [attr.transform]="optionsService.textTransform"
        [style.font-size]="optionsService.barFontSize + 'px'"
        [style.text-align]="textAlign(data)"
        *ngIf="shouldDisplayText(data)"
        class="bar-series-text"
      >
        <xhtml:div class="bar-series-div">
          <span [style.font-weight]="'bold'">
            {{ primaryText(data) }}
          </span>
          <span
            [attr.dx]="textPadding"
            *ngIf="shouldDisplaySecondaryText(data)"
          >
            {{ data.title }}
          </span>
        </xhtml:div>
      </svg:foreignObject>
    </svg:g>
  `
})
export class BarSeriesComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() data: any; // TODO: strongly type as TimelineEvent
  @Input() barWidth: (e: TimelineEvent) => number;
  @Input() barTransform: (e: TimelineEvent) => [number, number];
  @Input() timeScale: any;
  @Input() resourceScale: any;
  @Input() strokeWidth: number;
  @Input() activeId: string | number;
  @Input() selectedId: string | number;

  @Output() activate = new EventEmitter();
  @Output() deactivate = new EventEmitter();
  @Output() select = new EventEmitter();

  @ViewChild('barEl', { static: false })
  svgEl: ElementRef;

  textPadding = 5;

  transformX: number;
  transformY: number;
  dragging = false; // TODO: A hack

  constructor(
    public optionsService: OptionsService,
    private dragService: DragService,
    public zoomService: ZoomService
  ) {}

  ngOnInit() {
    this.initTransform();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.dragging) {
      return;
    }
    if (changes.resourceScale || changes.timeScale || changes.barTransform) {
      this.initTransform();
    }
  }

  transformString() {
    return `translate(${this.transformX}, ${this.transformY})`;
  }

  ngAfterViewInit(): void {
    const svg = select(this.svgEl.nativeElement);

    const dragged = () => {
      this.transformX = this.transformX + event.sourceEvent.movementX;
      this.transformY = this.transformY + event.sourceEvent.movementY;
      this.dragging = true;

      this.dragService.dragged({
        dataId: this.data.id,
        direction: event.sourceEvent.movementY,
        pointerDate: this.timeScale.invert(event.y),
        series: scaleBandInvert(this.resourceScale)(event.x),
        dragPoint: { x: this.transformX, y: this.transformY }
      });
    };

    const ended = () => {
      this.dragging = false;
      this.dragService.dragEnd();
    };

    const zoomFn = drag()
      .on('drag', dragged)
      .on('end', ended);

    svg.call(zoomFn);
  }

  getDivWidth(event: TimelineEvent) {
    return this.barWidth(event) - 2 * this.textPadding;
  }

  getDivHeight(event: TimelineEvent) {
    return this.barHeight(event) - 2 * this.textPadding;
  }

  primaryText(event: TimelineEvent): string {
    return (
      (this.optionsService.getBarOption(event.type, 'textDisplay') ===
        TextDisplay.Default &&
        event.type) ||
      event.title
    );
  }

  barHeight(event: TimelineEvent) {
    const height = this.timeScale(event.finish) - this.timeScale(event.start);
    return height > 0 ? height : 0.01;
  }

  shouldDisplayText(event: TimelineEvent): boolean {
    return this.barHeight(event) > this.optionsService.minimumTextDisplayHeight;
  }

  textAlign(event: TimelineEvent): string {
    return this.optionsService.getBarOption(event.type, 'textAlign') ===
      TextAlign.Left
      ? 'left'
      : 'right';
  }

  shouldDisplaySecondaryText(event: TimelineEvent): boolean {
    return (
      event.type &&
      this.optionsService.getBarOption(event.type, 'textDisplay') ===
        TextDisplay.Default
    );
  }

  private initTransform() {
    this.transformX = this.barTransform(this.data)[0];
    this.transformY = this.barTransform(this.data)[1];
  }

  @HostListener('mouseenter')
  onMouseEnter(): void {
    this.activate.emit(this.data);
  }

  @HostListener('mouseleave')
  onMouseLeave(): void {
    this.deactivate.emit(this.data);
  }
}
