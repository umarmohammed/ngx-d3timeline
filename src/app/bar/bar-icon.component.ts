import { Input, Component } from '@angular/core';
import { TimelineEvent } from '../timeline-event.model';
import { OptionsService } from '../options/options.service';
import { ZoomService } from '../zoom.service';

@Component({
  selector: 'g[ngx-d3timeline-bar-icon]',
  template: `
    <svg:g
      class="icon"
      [class.zooming]="zoomService.isZooming$ | async"
      *ngIf="optionsService.getIcon(data.type) as icon"
      [attr.transform]="barTransform(data)"
    >
      <svg:g
        ngx-d3timeline-bar-info-box
        [boxSize]="boxSize"
        [strokeWidth]="strokeWidth"
      ></svg:g>

      <svg:image
        class="icon-img"
        [attr.width]="iconSize"
        [attr.height]="iconSize"
        [attr.x]="iconMidPoint"
        [attr.y]="iconMidPoint"
        [attr.xlink:href]="icon"
      ></svg:image>
    </svg:g>
  `
})
export class BarIconComponent {
  @Input()
  data: TimelineEvent;

  @Input()
  barTransform: (e: TimelineEvent) => string;

  @Input()
  boxSize: number;

  @Input()
  strokeWidth: number;

  @Input()
  iconSize: number;

  @Input()
  iconMidPoint: number;

  constructor(
    public optionsService: OptionsService,
    public zoomService: ZoomService
  ) {}
}
