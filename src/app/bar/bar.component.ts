import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { TimelineEvent } from '../timeline-event.model';
import { OptionsService } from '../options/options.service';
import { Complete } from '../common/typescript-helpers';
import { Options } from '../options/options';
import { DragService } from '../drag.service';

@Component({
  selector: 'g[ngx-d3timeline-bar]',
  template: `
    <svg:defs>
      <svg:clipPath id="mask">
        <svg:rect
          x="0"
          y="0"
          [attr.width]="width"
          [attr.height]="height"
        ></svg:rect>
      </svg:clipPath>
    </svg:defs>
    <svg:g clip-path="url(#mask)">
      <svg:g
        ngx-d3timeline-bar-series
        *ngFor="let dp of data"
        class="bars-group"
        [class.drag-mode]="dragService.draggingEvent$ | async"
        [data]="dp"
        [barWidth]="barWidth"
        [barTransform]="barTransform"
        [timeScale]="timeScale"
        [resourceScale]="resourceScale"
        [strokeWidth]="options.strokeWidth"
        (activate)="activate.emit($event)"
        (deactivate)="deactivate.emit($event)"
        (select)="select.emit($event)"
        [activeId]="activeId"
        [selectedId]="selectedId"
      ></svg:g>

      <svg:g
        ngx-d3timeline-bar-info-boxes
        [activeId]="activeId"
        [selectedId]="selectedId"
        class="info-boxes-group"
        [data]="data"
        [iconTransform]="iconTransform"
        [barTransform]="barTransform"
        *ngFor="let data of orderedData"
        [options]="options"
        (activate)="activate.emit($event)"
        (deactivate)="deactivate.emit($event)"
        (select)="select.emit($event)"
      ></svg:g>

      <svg:g
        ngx-d3timeline-bar-placeholder
        *ngIf="dragService.placeholder$ | async as placeholder"
        class="bars-group"
        [data]="placeholder"
        [barWidth]="barWidth"
        [barTransform]="barTransform"
        [timeScale]="timeScale"
        [resourceScale]="resourceScale"
        [strokeWidth]="options.strokeWidth"
      ></svg:g>

      <svg:g
        ngx-d3timeline-bar-dragging
        *ngIf="dragService.draggingEvent$ | async as drag"
        class="bars-group"
        [drag]="drag"
        [barWidth]="barWidth"
        [barTransform]="barTransform"
        [timeScale]="timeScale"
        [resourceScale]="resourceScale"
        [strokeWidth]="options.strokeWidth"
      ></svg:g>
    </svg:g>
  `
})
export class BarComponent implements OnInit {
  @Input() data: TimelineEvent[];
  @Input() resources: string[];
  @Input() timeScale: any;
  @Input() resourceScale: any;
  @Input() options: Complete<Options>;
  @Input() activeId: string | number;
  @Input() selectedId: string | number;
  @Input() width: number;
  @Input() height: number;

  @Output() activate = new EventEmitter();
  @Output() deactivate = new EventEmitter();
  @Output() select = new EventEmitter();

  orderedData: TimelineEvent[];

  barTransform: (e: TimelineEvent) => [number, number];

  iconTransform: (e: TimelineEvent) => string;
  barWidth: (e: TimelineEvent) => number;

  constructor(
    public optionsService: OptionsService,
    public dragService: DragService
  ) {
    // TODO: hack
    this.dragService.placeholderWithData$.subscribe(s => {
      this.barTransform = (event: TimelineEvent) => [
        this.resourceScale(s.find(d => d.id === event.id).series) +
          this.optionsService.getBarTransformResourceAxisOffset(event.type),
        this.timeScale(s.find(d => d.id === event.id).start)
      ];

      this.iconTransform = (event: TimelineEvent) =>
        `translate(${this.resourceScale(s.find(d => d.id === event.id).series) +
          this.barWidth(event) +
          this.optionsService.getIconTransformResourceAxisOffset(
            event.type
          )}, ${this.timeScale(s.find(d => d.id === event.id).start)})`;
    });
  }

  ngOnInit(): void {
    this.orderedData = this.data.sort((a, b) => a.sequence - b.sequence);

    this.barWidth = (event: TimelineEvent) =>
      this.resourceScale.bandwidth() -
      this.optionsService.getBarWidthDiff(event.type);

    this.barTransform = (event: TimelineEvent) => [
      this.resourceScale(event.series) +
        this.optionsService.getBarTransformResourceAxisOffset(event.type),
      this.timeScale(event.start)
    ];

    this.iconTransform = (event: TimelineEvent) =>
      `translate(${this.resourceScale(event.series) +
        this.barWidth(event) +
        this.optionsService.getIconTransformResourceAxisOffset(
          event.type
        )}, ${this.timeScale(event.start)})`;
  }
}
