import { Component, Input } from '@angular/core';
import { TimelineEvent } from '../timeline-event.model';
import { OptionsService } from '../options/options.service';
import { ZoomService } from '../zoom.service';

@Component({
  selector: 'g[ngx-d3timeline-bar-sequence]',
  template: `
    <svg:g
      class="sequence"
      [class.zooming]="zoomService.isZooming$ | async"
      [attr.transform]="tranformString(data)"
      *ngIf="data.sequence"
    >
      <svg:g
        ngx-d3timeline-bar-info-box
        [boxSize]="boxSize"
        [strokeWidth]="strokeWidth"
      ></svg:g>
      <svg:text
        class="sequence-text"
        text-anchor="middle"
        fill="#fff"
        [attr.y]="boxSize / 2"
        dominant-baseline="central"
        [attr.transform]="optionsService.sequenceTextTransform"
      >
        {{ data.sequence }}
      </svg:text>
    </svg:g>
  `
})
export class BarSequenceComponent {
  @Input() data: TimelineEvent;
  @Input() barTransform: (e: TimelineEvent) => [number, number];
  @Input() boxSize: number;
  @Input() strokeWidth: number;

  constructor(
    public optionsService: OptionsService,
    public zoomService: ZoomService
  ) {}

  tranformString(event: TimelineEvent) {
    const translate = this.barTransform(event);
    return `translate(${translate[0]}, ${translate[1]})`;
  }
}
