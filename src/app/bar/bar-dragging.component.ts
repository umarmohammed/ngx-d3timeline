// TODO: this whole thing is a copy and paste job from bar-series.component.ts

import { Component, Input } from '@angular/core';
import { TimelineEvent } from '../timeline-event.model';
import { OptionsService } from '../options/options.service';
import { TextDisplay, TextAlign } from '../options/options';
import { TimelineDraggingEvent } from '../timeline-dragging-event.model';

@Component({
  selector: 'g[ngx-d3timeline-bar-dragging]',
  template: `
    <svg:g [attr.transform]="transformString()" [ngClass]="drag.data?.type">
      <svg:rect
        class="bar-series"
        [attr.width]="barWidth(drag.data)"
        [attr.height]="barHeight(drag.data)"
        [attr.stroke-width]="strokeWidth"
        [attr.transform]="optionsService.eventBarTransform"
      ></svg:rect>
      <svg:foreignObject
        [attr.width]="getDivWidth(drag.data)"
        [attr.height]="getDivHeight(drag.data)"
        [attr.transform]="optionsService.textTransform"
        [style.font-size]="optionsService.barFontSize + 'px'"
        [style.text-align]="textAlign(drag.data)"
        *ngIf="shouldDisplayText(drag.data)"
        class="bar-series-text"
      >
        <xhtml:div class="bar-series-div">
          <span [style.font-weight]="'bold'">
            {{ primaryText(drag.data) }}
          </span>
          <span
            [attr.dx]="textPadding"
            *ngIf="shouldDisplaySecondaryText(drag.data)"
          >
            {{ drag.data.title }}
          </span>
        </xhtml:div>
      </svg:foreignObject>
    </svg:g>
  `
})
export class BarDraggingComponent {
  @Input() data: any; // TODO: strongly type as TimelineEvent
  @Input() barWidth: (e: TimelineEvent) => number;
  @Input() barTransform: (e: TimelineEvent) => [number, number];
  @Input() timeScale: any;
  @Input() resourceScale: any;
  @Input() strokeWidth: number;
  @Input() drag: { dragging: TimelineDraggingEvent; data: TimelineEvent };

  textPadding = 5;

  constructor(public optionsService: OptionsService) {}

  transformString() {
    return `translate(${this.drag.dragging.dragPoint.x}, ${this.drag.dragging.dragPoint.y})`;
  }

  getDivWidth(event: TimelineEvent) {
    return this.barWidth(event) - 2 * this.textPadding;
  }

  getDivHeight(event: TimelineEvent) {
    return this.barHeight(event) - 2 * this.textPadding;
  }

  primaryText(event: TimelineEvent): string {
    return (
      (this.optionsService.getBarOption(event.type, 'textDisplay') ===
        TextDisplay.Default &&
        event.type) ||
      event.title
    );
  }

  barHeight(event: TimelineEvent) {
    const height = this.timeScale(event.finish) - this.timeScale(event.start);
    return height > 0 ? height : 0.01;
  }

  shouldDisplayText(event: TimelineEvent): boolean {
    return this.barHeight(event) > this.optionsService.minimumTextDisplayHeight;
  }

  textAlign(event: TimelineEvent): string {
    return this.optionsService.getBarOption(event.type, 'textAlign') ===
      TextAlign.Left
      ? 'left'
      : 'right';
  }

  shouldDisplaySecondaryText(event: TimelineEvent): boolean {
    return (
      event.type &&
      this.optionsService.getBarOption(event.type, 'textDisplay') ===
        TextDisplay.Default
    );
  }
}
