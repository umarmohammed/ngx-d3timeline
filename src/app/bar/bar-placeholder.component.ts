// TODO: this whole thing is a copy and paste job from bar series component

import { Component, Input } from '@angular/core';
import { TimelineEvent } from '../timeline-event.model';
import { OptionsService } from '../options/options.service';
import { TextDisplay, TextAlign } from '../options/options';

@Component({
  selector: 'g[ngx-d3timeline-bar-placeholder]',
  template: `
    <svg:g
      #barEl
      class="bar placeholder"
      [attr.transform]="transformString()"
      [ngClass]="data.type"
    >
      <svg:rect
        class="bar-series"
        [attr.width]="barWidth(data)"
        [attr.height]="barHeight(data)"
        [attr.stroke-width]="strokeWidth"
        [attr.transform]="optionsService.eventBarTransform"
      ></svg:rect>
    </svg:g>
  `
})
export class BarPlaceholderComponent {
  @Input() data: any; // TODO: strongly type as TimelineEvent
  @Input() barWidth: (e: TimelineEvent) => number;
  @Input() barTransform: (e: TimelineEvent) => [number, number];
  @Input() timeScale: any;
  @Input() resourceScale: any;
  @Input() strokeWidth: number;

  textPadding = 5;

  constructor(public optionsService: OptionsService) {}

  transformString() {
    const transform = this.barTransform(this.data);
    return `translate(${transform[0]}, ${transform[1]})`;
  }

  getDivWidth(event: TimelineEvent) {
    return this.barWidth(event) - 2 * this.textPadding;
  }

  getDivHeight(event: TimelineEvent) {
    return this.barHeight(event) - 2 * this.textPadding;
  }

  primaryText(event: TimelineEvent): string {
    return (
      (this.optionsService.getBarOption(event.type, 'textDisplay') ===
        TextDisplay.Default &&
        event.type) ||
      event.title
    );
  }

  barHeight(event: TimelineEvent) {
    const height = this.timeScale(event.finish) - this.timeScale(event.start);
    return height > 0 ? height : 0.01;
  }

  shouldDisplayText(event: TimelineEvent): boolean {
    return this.barHeight(event) > this.optionsService.minimumTextDisplayHeight;
  }

  textAlign(event: TimelineEvent): string {
    return this.optionsService.getBarOption(event.type, 'textAlign') ===
      TextAlign.Left
      ? 'left'
      : 'right';
  }

  shouldDisplaySecondaryText(event: TimelineEvent): boolean {
    return (
      event.type &&
      this.optionsService.getBarOption(event.type, 'textDisplay') ===
        TextDisplay.Default
    );
  }
}
