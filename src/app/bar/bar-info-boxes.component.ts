import {
  Component,
  Input,
  Output,
  EventEmitter,
  HostListener
} from '@angular/core';
import { TimelineEvent } from '../timeline-event.model';
import { Complete } from '../common/typescript-helpers';
import { Options } from '../options/options';
import { OptionsService } from '../options/options.service';

@Component({
  selector: 'g[ngx-d3timeline-bar-info-boxes]',
  template: `
    <svg:g
      class="info-boxes"
      [class.active]="data.id === activeId"
      [class.selected]="data.id === selectedId"
      (click)="select.emit(data)"
    >
      <svg:g
        ngx-d3timeline-bar-sequence
        class="sequence-group"
        *ngIf="options.showSequence"
        [data]="data"
        [barTransform]="barTransform"
        [boxSize]="options.infoBoxSize"
        [strokeWidth]="options.strokeWidth"
      ></svg:g>
      <svg:g
        ngx-d3timeline-bar-icon
        class="icon-group"
        *ngIf="options.showIcon"
        [data]="data"
        [barTransform]="iconTransform"
        [boxSize]="options.infoBoxSize"
        [strokeWidth]="options.strokeWidth"
        [iconSize]="options.iconSize"
        [iconMidPoint]="optionsService.iconMidPoint"
      ></svg:g>
    </svg:g>
  `
})
export class BarInfoBoxesComponent {
  @Input() data: TimelineEvent;
  @Input() barTransform: (e: TimelineEvent) => [number, number];
  @Input() iconTransform: (e: TimelineEvent) => string;
  @Input() options: Complete<Options>;
  @Input() activeId: string | number;
  @Input() selectedId: string | number;
  @Output() select = new EventEmitter();

  @Output() activate = new EventEmitter();
  @Output() deactivate = new EventEmitter();

  constructor(public optionsService: OptionsService) {}

  @HostListener('mouseenter')
  onMouseEnter(): void {
    this.activate.emit(this.data);
  }

  @HostListener('mouseleave')
  onMouseLeave(): void {
    this.deactivate.emit(this.data);
  }
}
