import { Component, Input } from '@angular/core';

@Component({
  selector: 'g[ngx-d3timeline-bar-info-box]',
  template: `
    <svg:rect
      class="info-box"
      [attr.width]="boxSize"
      [attr.height]="boxSize"
      [attr.stroke-width]="strokeWidth"
    ></svg:rect>
  `
})
export class BarInfoBoxComponent {
  @Input()
  boxSize: number;

  @Input()
  strokeWidth: number;
}
