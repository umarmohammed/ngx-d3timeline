import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { TimelineOldComponent } from './timeline-old.component';
import { TimeAxisComponent } from './axes/time-axis.component';
import { TimelineComponent } from './timeline.component';
import { ResourceAxisComponent } from './axes/resource-axis.component';
import { BarComponent } from './bar/bar.component';
import { BarSequenceComponent } from './bar/bar-sequence.component';
import { BarIconComponent } from './bar/bar-icon.component';
import { BarSeriesComponent } from './bar/bar-series.component';
import { BarInfoBoxComponent } from './bar/bar-info-box.component';
import { BarInfoBoxesComponent } from './bar/bar-info-boxes.component';
import { BarDraggingComponent } from './bar/bar-dragging.component';
import { BarPlaceholderComponent } from './bar/bar-placeholder.component';

@NgModule({
  declarations: [
    AppComponent,
    TimelineOldComponent,
    TimeAxisComponent,
    TimelineComponent,
    ResourceAxisComponent,
    BarComponent,
    BarSequenceComponent,
    BarIconComponent,
    BarSeriesComponent,
    BarInfoBoxComponent,
    BarInfoBoxesComponent,
    BarDraggingComponent,
    BarPlaceholderComponent
  ],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
