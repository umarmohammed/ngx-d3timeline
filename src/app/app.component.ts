import { Component, ViewEncapsulation } from '@angular/core';
import { DataService } from './data.service';
import { Options, TextDisplay, TextAlign } from './options/options';

@Component({
  selector: 'app-root',
  template: `
    <div
      style="position: absolute; top: 50px; left: 50px; right: 50px; bottom: 50px;"
    >
      <ngx-d3timeline
        [data]="data.timelineEvents$ | async"
        [options]="options"
        [activeId]="activeId"
        [selectedId]="selectId"
        (activate)="onEvent({ activate: $event })"
        (deactivate)="onEvent({ deactivate: $event })"
        (select)="onEvent({ select: $event })"
      ></ngx-d3timeline>
    </div>
  `,
  styles: [
    `
      .bar.Resting .bar-series {
        fill: #bcd1d7;
        stroke: #76858d;
        rx: 5;
      }

      .bar.Driving .bar-series {
        fill: #dadee0;
        stroke: #4e585e;
      }

      .bar.Waiting .bar-series {
        fill: #bcd1d7;
        stroke: #76858d;
        rx: 5;
      }

      .bar.DriveBreak .bar-series {
        fill: #bcd1d7;
        stroke: #76858d;
        rx: 5;
      }

      .bar.Handling .bar-series {
        fill: #00a1c5;
        stroke: #006176;
        rx: 5;
      }

      .info-box {
        rx: 5;
      }

      text {
        font: 12px sans-serif;
      }

      .bar-series-text {
        font-family: sans-serif;
      }

      .bar.active .bar-series {
        fill: orange;
      }

      .info-boxes.active .info-box {
        fill: orange;
      }

      .resource-axis-group.active .resource-rect {
        fill: orange;
        fill-opacity: 0.3;
        stroke: orange;
      }

      .resource-axis-group.active .resource-text-rect {
        fill: orange;
      }

      .bar.selected .bar-series {
        fill: blue;
      }

      .info-boxes.selected .info-box {
        fill: blue;
      }

      .resource-axis-group.selected .resource-rect {
        fill: blue;
        fill-opacity: 0.3;
        stroke: blue;
      }

      .resource-axis-group.selected .resource-text-rect {
        fill: blue;
      }
    `
  ],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class AppComponent {
  options: Options = {
    strokeWidth: 3,
    innerPadding: 5,
    showSequence: true,
    showIcon: true,
    showSecondAxis: true,
    showGridLines: true,
    type: {
      Driving: {
        bar: {
          padding: 5,
          textDisplay: TextDisplay.Title,
          textAlign: TextAlign.Right,
          zIndex: -1
        }
      },
      Waiting: {
        icon: 'assets/cup.svg'
      },
      DriveBreak: {
        icon: 'assets/cup.svg'
      },
      Handling: {
        icon: 'assets/truck.svg'
      },
      Resting: {
        icon: 'assets/cup.svg'
      }
    }
  };

  activeId: string | number;
  selectId: string | number;

  constructor(public data: DataService) {}

  onEvent(event: any) {
    console.log(event);
    if (event.activate) {
      if (typeof event.activate === 'string') {
        this.activeId = event.activate;
      } else {
        this.activeId = event.activate && event.activate.id;
      }
    }

    if (event.deactivate) {
      this.activeId = null;
    }

    if (event.select) {
      if (typeof event.select === 'string') {
        this.selectId = event.select;
      } else {
        this.selectId = event.select && event.select.id;
      }
    }
  }
}
