import { TimelineEvent, Point } from './timeline-event.model';

export interface TimelineDraggingEvent {
  dataId: string | number;
  pointerDate: Date;
  series: string;
  direction: number;
  dragPoint: Point;
}
