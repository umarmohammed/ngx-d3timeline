import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  AfterViewInit,
  SimpleChanges,
  OnChanges
} from '@angular/core';
import { Point, TimelineEvent } from './timeline-event.model';
import * as d3 from 'd3';
import { DataService } from './data.service';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-timeline-old',
  template: `
    <svg #foo width="960" height="500"></svg>
    <button class="reset">Reset</button>
  `,
  styles: [
    `
      :host /deep/ .yAxisGroup line {
        stroke: rgb(118, 133, 141);
        stroke-dasharray: 10 5;
      }

      :host /deep/ .yAxisGroup path {
        stroke: rgb(118, 133, 141);
      }

      :host /deep/ .bar text {
        font: 12px sans-serif;
      }

      :host /deep/ .bar rect {
        stroke-width: 3px;
      }
    `
  ]
})
export class TimelineOldComponent implements AfterViewInit, OnChanges {
  @ViewChild('foo', { static: false })
  private foo: ElementRef;

  private initSubject = new BehaviorSubject(false);

  trucks = ['Truck_768', 'Truck_763'];

  @Input()
  data: TimelineEvent[];

  y: any;
  x: any;
  barWidth: number;
  svg: any;
  margin = 50;
  barGroup: any;
  newYScale: any;
  rect: any;
  text: any;

  private foobar$ = combineLatest(
    this.initSubject,
    this.dataService.timelineEvents$
  ).pipe(
    filter(([ready]) => ready),
    map(([, data]) => data)
  );

  constructor(private dataService: DataService) {}

  foob(point: Point) {
    if (point) {
      const hackyOffsetsMarginsEtc = 255;
      const x = this.x.invert(point.x - hackyOffsetsMarginsEtc);
      if (x > 0 && x < 2) {
        this.dataService.addData(Math.floor(x));
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!this.svg) {
      return;
    }

    this.barGroup.selectAll('g').remove();
    const bar = this.barGroup
      .selectAll('.bar-group')
      .data(this.data)
      .enter()
      .append('g')
      .attr('class', 'bar');

    this.rect = bar
      .append('rect')
      .attr(
        'transform',
        d =>
          'translate(' +
          this.x(this.trucks.indexOf(d.vehicleIdentifier)) +
          ',' +
          this.y(d.start) +
          ')'
      )
      .attr('width', this.barWidth)
      .attr('height', d => this.y(d.finish) - this.y(d.start))
      .attr('stroke', d => this.getColor(d).stroke)
      .attr('fill', d => this.getColor(d).fill)
      .attr('rx', 5);

    this.text = bar
      .append('text')
      .attr(
        'transform',
        d =>
          'translate(' +
          this.x(this.trucks.indexOf(d.vehicleIdentifier)) +
          ', ' +
          this.y(d.start) +
          ')'
      )
      .attr('dy', '1em')
      .attr('dx', '2em')
      .text(d => (this.y(d.finish) - this.y(d.start) > 30 ? d.type : ''));

    if (!this.newYScale) {
      return;
    }

    this.rect.attr(
      'transform',
      d =>
        'translate(' +
        this.x(this.trucks.indexOf(d.vehicleIdentifier)) +
        ', ' +
        this.newYScale(d.start) +
        ')'
    );
    this.rect.attr(
      'height',
      d => this.newYScale(d.finish) - this.newYScale(d.start)
    );

    this.text
      .text(d =>
        this.newYScale(d.finish) - this.newYScale(d.start) > 20 ? d.type : ''
      )
      .attr(
        'transform',
        d =>
          'translate(' +
          this.x(this.trucks.indexOf(d.vehicleIdentifier)) +
          ', ' +
          this.newYScale(d.start) +
          ')'
      );
  }

  ngAfterViewInit() {
    this.dataService.pointSubject.subscribe(point => this.foob(point));

    const onZoom = d3.zoom().on('zoom', zoomed.bind(this));

    this.svg = d3.select(this.foo.nativeElement);

    this.svg.select('g').remove();

    const width = +this.svg.attr('width') - this.margin * 2;
    const height = +this.svg.attr('height');

    this.barWidth = width / this.trucks.length;

    const dataGroup = this.svg
      .append('g')
      .attr('class', 'data-group')
      .attr('transform', 'translate(' + this.margin + ', ' + this.margin + ')');

    this.y = d3
      .scaleTime()
      .domain(d3.extent(this.data, d => d.start))
      .range([0, height]);

    this.x = d3
      .scaleLinear()
      .domain([0, this.trucks.length])
      .range([0, width]);

    const xAxisGroup = dataGroup.append('g').attr('class', 'xAxisGroup');

    const xAxis = d3.axisBottom(this.x).tickValues([]);

    const yAxisGroup = dataGroup.append('g').attr('class', 'yAxisGroup');

    const yAxisGroupRight = dataGroup
      .append('g')
      .attr('class', 'yAxisGroupRight')
      .attr('transform', 'translate(' + width + ',0)');

    const yAxis = d3.axisLeft(this.y).tickSize(-width);

    const yAxisRight = d3.axisRight(this.y).tickSize(0);

    yAxis(yAxisGroup);
    yAxisRight(yAxisGroupRight);
    xAxis(xAxisGroup);

    this.barGroup = dataGroup.append('g').attr('class', 'bar-group');

    const bar = this.barGroup
      .selectAll('.bar-group')
      .data(this.data)
      .enter()
      .append('g')
      .attr('class', 'bar');

    bar.exit().remove();

    const trucksGroup = dataGroup
      .selectAll('.data-group')
      .data(this.trucks)
      .enter()
      .append('g')
      .attr('class', 'route-header')
      .append('text')
      .attr(
        'transform',
        (d, i) => 'translate(' + (this.x(i) + this.barWidth / 2) + ', 0)'
      )
      .text(d => d);

    this.rect = bar
      .append('rect')
      .attr(
        'transform',
        d =>
          'translate(' +
          this.x(this.trucks.indexOf(d.vehicleIdentifier)) +
          ',' +
          this.y(d.start) +
          ')'
      )
      .attr('width', this.barWidth)
      .attr('height', d => this.y(d.finish) - this.y(d.start))
      .attr('stroke', d => this.getColor(d).stroke)
      .attr('fill', d => this.getColor(d).fill)
      .attr('rx', 5);

    this.text = bar
      .append('text')
      .attr(
        'transform',
        d =>
          'translate(' +
          this.x(this.trucks.indexOf(d.vehicleIdentifier)) +
          ', ' +
          this.y(d.start) +
          ')'
      )
      .attr('dy', '1em')
      .attr('dx', '2em')
      .text(d => (this.y(d.finish) - this.y(d.start) > 30 ? d.type : ''));

    onZoom(this.svg);

    d3.select('button.reset').on('click', resetted.bind(this));

    function resetted() {
      this.svg
        .transition()
        .duration(750)
        .call(onZoom.transform, d3.zoomIdentity);
    }

    function zoomed() {
      yAxisGroup.call(yAxis.scale(d3.event.transform.rescaleY(this.y)));
      yAxisGroupRight.call(
        yAxisRight.scale(d3.event.transform.rescaleY(this.y))
      );
      this.newYScale = d3.event.transform.rescaleY(this.y);
      this.rect.attr(
        'transform',
        d =>
          'translate(' +
          this.x(this.trucks.indexOf(d.vehicleIdentifier)) +
          ', ' +
          this.newYScale(d.start) +
          ')'
      );
      this.rect.attr(
        'height',
        d => this.newYScale(d.finish) - this.newYScale(d.start)
      );

      this.text
        .text(d =>
          this.newYScale(d.finish) - this.newYScale(d.start) > 20 ? d.type : ''
        )
        .attr(
          'transform',
          d =>
            'translate(' +
            this.x(this.trucks.indexOf(d.vehicleIdentifier)) +
            ', ' +
            this.newYScale(d.start) +
            ')'
        );
    }
  }

  private getColor(data) {
    switch (data.type) {
      case 'Driving':
        return {
          fill: 'rgba(218, 222, 224, 1)',
          stroke: 'rgba(78, 88, 94, 1)'
        };
      case 'DriveBreak':
      case 'Resting':
      case 'Waiting':
        return {
          fill: 'rgba(188, 209, 215, 1)',
          stroke: 'rgba(118, 133, 141, 1)'
        };
      case 'Handling':
        return {
          fill: 'rgba(0, 161, 197, 1)',
          stroke: 'rgba(0, 97, 118, 1)'
        };
      default:
        return {
          fill: 'rgba(0, 161, 197, 1)',
          stroke: 'rgba(0, 97, 118, 1)'
        };
    }
  }
}
