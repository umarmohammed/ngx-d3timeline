import { Observable, of, BehaviorSubject, combineLatest } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import {
  TimelineEvent,
  mockData,
  Point,
  getMinStartTime
} from './timeline-event.model';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DataService {
  private addedDataSubject = new BehaviorSubject<TimelineEvent>(null);
  pointSubject = new BehaviorSubject<Point>(null);

  notLoadsSubject = new BehaviorSubject<number[]>([0]);

  droppedSubject = new BehaviorSubject<boolean>(false);

  dropPoint$ = combineLatest(this.pointSubject, this.droppedSubject).pipe(
    filter(([, dropped]) => dropped),
    map(([point, _]) => point)
  );

  timelineEvents$: Observable<TimelineEvent[]> = combineLatest(
    of(mockData),
    this.addedDataSubject
  ).pipe(map(([events, added]) => (added ? [...events, added] : events)));

  addData(index: number) {
    const vehicleIdentifier = index === 0 ? 'Truck_768' : 'Truck_763';
    this.addedDataSubject.next({
      ...this.createPrependEvent(vehicleIdentifier),
      type: 'Handling',
      series: vehicleIdentifier,
      title: ''
    });
  }

  private createPrependEvent(vid: string): { start: Date; finish: Date } {
    const finish = getMinStartTime(vid);
    const start = new Date(finish);
    start.setHours(start.getHours() - 1);
    return { start, finish };
  }
}
