import {
  Component,
  Input,
  OnInit,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { Complete } from '../common/typescript-helpers';
import { Options } from '../options/options';

@Component({
  selector: 'g[ngx-d3timeline-time-axis]',
  template: `
    <svg:g class="time-axis-group">
      <svg:g
        *ngFor="let tick of ticks"
        class="tick"
        [attr.transform]="tickTransform(tick)"
      >
        <title>{{ tickFormat(tick) }}</title>
        <svg:text
          stroke-width="0.01"
          text-anchor="end"
          [style.font-size]="'12px'"
          x="-3"
        >
          {{ tickFormat(tick) }}
        </svg:text>
      </svg:g>
      <ng-container *ngIf="options.showGridLines">
        <svg:g
          *ngFor="let tick of ticks"
          class="tick"
          [attr.transform]="tickTransform(tick)"
        >
          <svg:line
            class="time-grid-line"
            [attr.x2]="width"
            [attr.stroke]="'black'"
          />
        </svg:g>
      </ng-container>
      <svg:g
        *ngIf="options.showSecondAxis"
        [attr.transform]="secondAxisTransform"
        class="second-axis"
      >
        <svg:g
          *ngFor="let tick of ticks"
          class="tick"
          [attr.transform]="tickTransform(tick)"
        >
          <title>{{ tickFormat(tick) }}</title>
          <svg:text stroke-width="0.01" [style.font-size]="'12px'" x="3">
            {{ tickFormat(tick) }}
          </svg:text>
        </svg:g>
      </svg:g>
    </svg:g>
  `
})
export class TimeAxisComponent implements OnInit, OnChanges {
  ticks: any;
  tickFormat: (o: any) => any;
  secondAxisTransform: string;

  @Input()
  width: number;

  @Input()
  timeScale: any;

  @Input()
  startTimeScale: any;

  @Input()
  options: Complete<Options>;

  ngOnInit(): void {
    this.timeScale = this.timeScale || this.startTimeScale;
    this.tickFormat = this.startTimeScale.tickFormat.apply(this.startTimeScale);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.timeScale) {
      this.ticks = changes.timeScale.currentValue.ticks.apply(
        this.startTimeScale
      );
    }

    // TODO: bind to observable
    if (this.width) {
      this.secondAxisTransform = `translate(${this.width}, 0)`;
    }
  }

  tickTransform(tick): string {
    return `translate(0,${this.timeScale(tick)})`;
  }
}
