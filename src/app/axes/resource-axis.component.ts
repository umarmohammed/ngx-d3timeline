import {
  Component,
  Input,
  OnInit,
  Output,
  HostListener,
  EventEmitter,
  SimpleChanges,
  OnChanges
} from '@angular/core';
import { ResourceOptions } from '../options/options';
import { Complete } from '../common/typescript-helpers';

@Component({
  selector: 'g[ngx-d3timeline-resource-axis]',
  template: `
    <svg:g
      class="resource-axis-group"
      [class.active]="activeId === resource"
      [class.selected]="selectedId === resource"
      (click)="select.emit(resource)"
    >
      <title>{{ resource }}</title>
      <svg:rect
        class="resource-rect"
        [attr.height]="rectHeight"
        [attr.width]="resourceScale.bandwidth()"
        fill-opacity="0"
        stroke="black"
        [attr.transform]="outlineTransform(resource)"
      ></svg:rect>
      <svg:rect
        class="resource-text-rect"
        [attr.height]="resourceOptions.titleHeight"
        [attr.width]="resourceScale.bandwidth() - 2 * padding"
        [attr.transform]="tickRectTransform(resource)"
        fill="black"
      ></svg:rect>
      <svg:text
        [attr.transform]="tickTransform(resource)"
        stroke-width="0.01"
        [attr.text-anchor]="'middle'"
        [style.font-size]="'12px'"
        fill="white"
        [attr.y]="resourceOptions.titleHeight / 2"
        dominant-baseline="central"
      >
        {{ resource }}
      </svg:text>
    </svg:g>
  `
})
export class ResourceAxisComponent implements OnInit, OnChanges {
  @Input() resourceScale: any;
  @Input() resource: string;
  @Input() height: number;
  @Input() resourceOptions: Complete<ResourceOptions>;
  @Input() padding: number;
  @Input() activeId: string;
  @Input() selectedId: string;

  @Output() activate = new EventEmitter();
  @Output() deactivate = new EventEmitter();
  @Output() select = new EventEmitter();

  rectHeight: number;

  ngOnInit(): void {
    this.rectHeight = this.height - this.resourceOptions.outlineStrokeWidth / 2;
  }

  // TODO: bind to obeservables
  ngOnChanges() {
    this.rectHeight =
      this.height && this.height - this.resourceOptions.outlineStrokeWidth / 2;
  }

  tickTransform(resource: string): string {
    const y = this.padding;
    return `translate(${this.resourceScale(resource) +
      this.resourceScale.bandwidth() / 2}, ${y})`;
  }

  outlineTransform(resource: string): string {
    return `translate(${this.resourceScale(resource)}, 0)`;
  }

  tickRectTransform(resource: string): string {
    // TODO: Performance calculate once per resource
    const x = this.resourceScale(resource) + this.padding;
    const y = this.padding;
    return `translate(${x},${y})`;
  }

  @HostListener('mouseenter')
  onMouseEnter(): void {
    this.activate.emit(this.resource);
  }

  @HostListener('mouseleave')
  onMouseLeave(): void {
    this.deactivate.emit(this.resource);
  }
}
