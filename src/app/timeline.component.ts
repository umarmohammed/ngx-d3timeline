import {
  Component,
  Input,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnChanges,
  SimpleChanges,
  ViewEncapsulation,
  Output,
  EventEmitter,
  ChangeDetectorRef,
  OnDestroy
} from '@angular/core';
import { TimelineEvent } from './timeline-event.model';
import { scaleBand, scaleTime } from 'd3-scale';
import { extent } from 'd3-array';
import { select, event } from 'd3-selection';
import { zoom } from 'd3-zoom';
import { Options } from './options/options';
import { OptionsService } from './options/options.service';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ZoomService } from './zoom.service';

@Component({
  selector: 'ngx-d3timeline',
  template: `
    <svg
      class="ngx-d3timeline"
      [attr.width]="width"
      [attr.height]="height"
      #timelineEl
    >
      <svg:g [attr.transform]="svgTransform">
        <svg:g
          ngx-d3timeline-time-axis
          [width]="viewWidth"
          [startTimeScale]="startTimeScale"
          [timeScale]="timeScale"
          [attr.transform]="timelineTransform"
          [options]="optionsService.options"
        ></svg:g>
        <svg:g
          *ngFor="let resource of resources"
          class="resource"
          ngx-d3timeline-resource-axis
          [resource]="resource"
          [resourceScale]="resourceScale"
          [height]="viewHeight"
          [padding]="optionsService.options.innerPadding"
          [resourceOptions]="optionsService.options.resource"
          (activate)="activate.emit($event)"
          (deactivate)="deactivate.emit($event)"
          (select)="select.emit($event)"
          [activeId]="seriesId"
          [selectedId]="selectedSeriesId"
        >
          >
        </svg:g>
        <svg:g
          ngx-d3timeline-bar
          (activate)="activate.emit($event)"
          (deactivate)="deactivate.emit($event)"
          (select)="select.emit($event)"
          [resources]="resources"
          [data]="zOrderedData"
          [resourceScale]="resourceScale"
          [timeScale]="timeScale"
          [attr.transform]="timelineTransform"
          [options]="optionsService.options"
          [activeId]="eventId"
          [selectedId]="selectedEventId"
          [width]="width"
          [height]="height"
        ></svg:g>
      </svg:g>
    </svg>
  `,
  styles: [
    `
      .ngx-d3timeline .time-grid-line {
        stroke: #76858d;
        stroke-dasharray: 10 5;
      }
      .ngx-d3timeline .bar-series {
        fill: #eee;
        stroke: #000;
        cursor: pointer;
      }
      .ngx-d3timeline .bar-series-text {
        cursor: pointer;
      }

      .ngx-d3timeline .bar-series-div {
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
      }
      .ngx-d3timeline .info-box {
        fill: #666;
        stroke: #111;
        cursor: pointer;
      }

      .ngx-d3timeline .icon-img {
        cursor: pointer;
      }

      .ngx-d3timeline .sequence-text {
        cursor: pointer;
      }

      .ngx-d3timeline .resource {
        cursor: pointer;
      }

      .ngx-d3timeline .bar.placeholder {
        fill-opacity: 0;
        transition: none;
      }

      .ngx-d3timeline .bar {
        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);
      }

      .ngx-d3timeline .zooming {
        transition: none;
      }

      /* the bar hidden behind the one in drag layer */
      .ngx-d3timeline .bar.dragging {
        transition: none;
      }

      .ngx-d3timeline .sequence {
        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);
      }

      .ngx-d3timeline .sequence.zooming {
        transition: none;
      }

      .ngx-d3timeline .icon {
        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);
      }

      .ngx-d3timeline .icon.zooming {
        transition: none;
      }
    `
  ],
  encapsulation: ViewEncapsulation.None
})
export class TimelineComponent
  implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  @Input() data: TimelineEvent[];
  @Input() options: Options;
  @Input() activeId: string | number;
  @Input() selectedId: string | number;
  @Input() view: [number, number];

  @Output() activate = new EventEmitter();
  @Output() deactivate = new EventEmitter();
  @Output() select = new EventEmitter();

  timeScale: any;
  resourceScale: any;
  svgTransform: string;
  timelineTransform: string;
  viewWidth: number;
  viewHeight: number;

  width: number;
  height: number;
  margin = 50;

  seriesId: string;
  eventId: string | number;

  selectedSeriesId: string;
  selectedEventId: string | number;

  startTimeScale: any;

  zOrderedData: TimelineEvent[];

  resizeSubscription: Subscription;

  private foo;

  @ViewChild('timelineEl', { static: false })
  svgEl: ElementRef;

  constructor(
    public optionsService: OptionsService,
    private chartElement: ElementRef,
    private cd: ChangeDetectorRef,
    private zoomService: ZoomService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    this.update();

    if (changes.options) {
      this.optionsService.options =
        changes.options && changes.options.currentValue;
    }

    if (changes.activeId) {
      this.seriesId = null;
      this.eventId = null;
      this.seriesId =
        this.resources &&
        this.resources.find(seriesId => seriesId === this.activeId);
      if (!this.seriesId) {
        const activeEvent = this.data.find(d => d.id === this.activeId);
        this.seriesId = activeEvent && activeEvent.series;
        this.eventId = activeEvent && activeEvent.id;
      }
    }

    // TODO: refactor
    if (changes.selectedId) {
      this.selectedSeriesId = null;
      this.selectedEventId = null;
      this.selectedSeriesId =
        this.resources &&
        this.resources.find(seriesId => seriesId == this.selectedId);
      if (!this.selectedSeriesId) {
        const selectedEvent = this.data.find(d => d.id === this.selectedId);
        this.selectedSeriesId = selectedEvent && selectedEvent.series;
        this.selectedEventId = selectedEvent && selectedEvent.id;
      }
    }
  }

  ngOnInit(): void {
    // TODO: tidy up
    this.zOrderedData = this.data.sort(
      (a, b) =>
        this.optionsService.getBarOption(a.type, 'zIndex') -
        this.optionsService.getBarOption(b.type, 'zIndex')
    );
    this.startTimeScale = this.getTimeScale();
    this.viewWidth = this.width - 2 * this.margin;
    this.timeScale = this.getTimeScale();

    this.resourceScale = this.getResourceScale();
    this.svgTransform = `translate(${this.margin}, ${this.margin})`;

    this.viewHeight = this.height - this.margin;

    this.timelineTransform = `translate(0,${this.optionsService.options.resource
      .titleHeight +
      2 * this.optionsService.options.innerPadding})`;
  }

  ngAfterViewInit(): void {
    this.bindWindowResizeEvent();

    const svg = select(this.svgEl.nativeElement);

    const zoomed = () => {
      this.timeScale = event.transform.rescaleY(this.startTimeScale);
    };

    const zoomStart = () => {
      this.zoomService.zoomstart();
    };

    const zoomEnd = () => {
      this.zoomService.zoomend();
    };

    const zoomFn = zoom()
      .on('zoom', zoomed)
      .on('start', zoomStart)
      .on('end', zoomEnd);

    svg.call(zoomFn);
  }

  ngOnDestroy(): void {
    if (this.resizeSubscription) {
      this.resizeSubscription.unsubscribe();
    }
  }

  private bindWindowResizeEvent() {
    const source = fromEvent(window, 'resize');
    const subscription = source.pipe(debounceTime(200)).subscribe(e => {
      this.update();
      if (this.cd) {
        this.cd.markForCheck();
      }
    });
    this.resizeSubscription = subscription;
  }

  get resources(): string[] {
    // TODO: create helper called get unique
    return (
      this.zOrderedData &&
      this.zOrderedData
        .map(d => d.series)
        .filter((value, index, array) => array.indexOf(value) === index)
    );
  }

  // TODO: refactor
  private update() {
    if (this.view) {
      this.width = this.view[0];
      this.height = this.view[1];
    } else {
      const dims = this.getContainerDims();
      if (dims) {
        this.width = dims.width;
        this.height = dims.height;
      }
    }

    // default values if width or height are 0 or undefined
    if (!this.width) {
      this.width = 600;
    }

    if (!this.height) {
      this.height = 400;
    }

    this.width = Math.floor(this.width);
    this.height = Math.floor(this.height);

    this.startTimeScale = this.getTimeScale();
    this.viewWidth = this.width - 2 * this.margin;

    this.resourceScale = this.getResourceScale();

    this.viewHeight = this.height - this.margin;

    if (this.cd) {
      this.cd.markForCheck();
    }
  }

  private getContainerDims(): any {
    let width;
    let height;
    const hostElem = this.chartElement.nativeElement;

    if (hostElem.parentNode !== null) {
      // Get the container dimensions
      const dims = hostElem.parentNode.getBoundingClientRect();
      width = dims.width;
      height = dims.height;
    }

    if (width && height) {
      return { width, height };
    }

    return null;
  }

  private getTimeScale(): any {
    if (!this.zOrderedData) {
      return;
    }

    const domain = extent(this.zOrderedData, d => d.start);
    return scaleTime()
      .range([0, this.height])
      .domain(domain);
  }

  private getResourceScale(): any {
    if (!this.resources) {
      return;
    }

    const numResources = this.resources.length;
    const maxResourceWidth =
      numResources * this.optionsService.options.resource.maxWidth;

    const resourceWidth = Math.min(maxResourceWidth, this.viewWidth);

    const spacing =
      numResources /
      (resourceWidth / this.optionsService.options.seriesMargin + 1);

    return scaleBand()
      .range([0, resourceWidth])
      .paddingInner(spacing)
      .domain(this.resources);
  }
}
