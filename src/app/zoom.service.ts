import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ZoomService {
  private isZoomingSubject = new Subject<boolean>();

  isZooming$ = this.isZoomingSubject.asObservable();

  zoomstart() {
    this.isZoomingSubject.next(true);
  }

  zoomend() {
    this.isZoomingSubject.next(false);
  }
}
