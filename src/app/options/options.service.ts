import { Injectable } from '@angular/core';
import { Complete, mergeDeep } from '../common/typescript-helpers';
import { Options, TextAlign } from './options';
import { defaultOptions } from './default-options';

@Injectable({ providedIn: 'root' })
export class OptionsService {
  private _options: Complete<Options>;

  get options(): Complete<Options> {
    return this._options;
  }

  set options(value: Complete<Options>) {
    this._options = mergeDeep(defaultOptions, value);
  }

  getBarOption(eventType: string, prop: string) {
    return (
      (eventType &&
        this.options.type[eventType] &&
        this.options.type[eventType].bar &&
        this.options.type[eventType].bar[prop]) ||
      this.options.bar[prop]
    );
  }

  getIcon(eventType: string): string {
    return this.options.type[eventType] && this.options.type[eventType].icon;
  }

  getBarTransformResourceAxisOffset(eventType: string): number {
    return (
      this.options.strokeWidth / 2 +
      this.options.innerPadding +
      this.getBarOption(eventType, 'padding')
    );
  }

  getIconTransformResourceAxisOffset(eventType: string): number {
    return (
      this.getBarTransformResourceAxisOffset(eventType) +
      this.iconWidth +
      this.options.strokeWidth
    );
  }

  getBarWidthDiff(eventType: string): number {
    return (
      this.options.strokeWidth +
      2 * this.options.innerPadding +
      2 * this.getBarOption(eventType, 'padding') +
      this.sequenceWidth +
      this.iconWidth
    );
  }

  get sequenceWidth(): number {
    return this.options.showSequence
      ? this.options.infoBoxSize + this.options.strokeWidth
      : 0;
  }

  get iconWidth(): number {
    return this.options.showIcon
      ? this.options.infoBoxSize + this.options.strokeWidth
      : 0;
  }

  get textPaddingOffset(): number {
    return this.options.bar.textPadding + this.options.strokeWidth;
  }

  get textTransform() {
    const x =
      this.sequenceWidth + this.textPaddingOffset - this.options.strokeWidth;

    const y = this.options.bar.textPadding;

    return `translate(${x}, ${y})`;
  }

  get eventBarTransform(): string {
    return this.options.showSequence
      ? `translate(${this.options.infoBoxSize + this.options.strokeWidth}, 0)`
      : null;
  }

  get sequenceTextTransform(): string {
    return `translate(${this.options.infoBoxSize / 2}, 0)`;
  }

  get barFontSize(): number {
    return this.options.bar.fontSize;
  }

  get iconMidPoint(): number {
    return (this.options.infoBoxSize - this.options.iconSize) / 2;
  }

  get minimumTextDisplayHeight(): number {
    return this.barFontSize + 2 * this.options.bar.textPadding + 2; // TODO: 2 is a random padding
  }
}
