export enum TextDisplay {
  Default,
  Title
}

export enum TextAlign {
  Left,
  Right
}

export interface TypeOptions {
  bar?: BarOptions;
  icon?: string;
}

export interface BarOptions {
  padding?: number;
  textDisplay?: TextDisplay;
  textAlign?: TextAlign;
  zIndex?: number;
  fontSize?: number;
  textPadding?: number;
}

export interface ResourceOptions {
  titleHeight?: number;
  outlineStrokeWidth?: number;
  maxWidth?: number;
}

export interface Options {
  strokeWidth?: number;
  seriesMargin?: number;
  innerPadding?: number;
  showSequence?: boolean;
  showIcon?: boolean;
  iconSize?: number;
  infoBoxSize?: number;
  showSecondAxis?: boolean;
  showGridLines?: boolean;
  type?: { [key: string]: TypeOptions };
  bar?: BarOptions;
  resource?: ResourceOptions;
}
