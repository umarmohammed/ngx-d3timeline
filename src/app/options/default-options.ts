import { Complete } from '../common/typescript-helpers';

import { Options, TextAlign, TextDisplay } from './options';

export const defaultOptions: Complete<Options> = {
  seriesMargin: 0,
  strokeWidth: 1,
  showSequence: false,
  showIcon: false,
  showSecondAxis: false,
  showGridLines: false,
  iconSize: 20,
  innerPadding: 0,
  infoBoxSize: 30,
  bar: {
    padding: 0,
    textAlign: TextAlign.Left,
    textDisplay: TextDisplay.Default,
    zIndex: 1,
    fontSize: 12,
    textPadding: 5
  },
  resource: {
    titleHeight: 20,
    outlineStrokeWidth: 1,
    maxWidth: 400
  },
  type: {}
};
